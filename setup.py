import os
from setuptools import setup

with open(os.path.join('ndscope', '__version__.py')) as f:
    exec(f.read())

with open('README.md', 'rb') as f:
    longdesc = f.read().decode().strip()

setup(
    name='ndscope',
    version=__version__,
    description="Next-generation NDS time series plotting",
    long_description=longdesc,
    long_description_content_type='text/markdown',
    author='Jameson Graef Rollins',
    author_email='jameson.rollins@ligo.org',
    url='https://git.ligo.org/cds/ndscope',
    license='GPL-3.0-or-later',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        ('License :: OSI Approved :: '
         'GNU General Public License v3 or later (GPLv3+)'),
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],

    install_requires=[
        'PyQt5',
        'pyqtgraph',
        'python-dateutil',
        'nds2-client',
        'numpy',
        'gpstime',
    ],

    packages=[
        'ndscope',
        'ndscope.test',
    ],
    scripts=[
        'bin/ndscope'
    ],

    package_data={
        'ndscope': ['*.ui'],
        'ndscope.test': ['templates/*'],
    },
)
