# -*- coding: utf-8 -*-
import os
import logging

import numpy as np

import pyqtgraph as pg
try:
    from qtpy import QtGui, QtWidgets, QtCore
    from qtpy.QtWidgets import QStyle, QFileDialog
    from qtpy.QtGui import QImage, QPainter
    from qtpy import uic
except ImportError:
    from PyQt5 import QtGui, QtWidgets, QtCore
    from PyQt5.QtGui import QStyle, QFileDialog
    from PyQt5.QtGui import QImage, QPainter
    from PyQt5 import uic

from . import __version__
from . import const
from . import util
from .data import DataStore
from .plot import NDScopePlot
from .trigger import Trigger
from .cursors import Crosshair, TCursors, YCursors


logger = logging.getLogger('SCOPE')


##################################################
# CONFIG


def set_background(color='k'):
    if color == 'w':
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')


if os.getenv('ANTIALIAS'):
    pg.setConfigOption('antialias', True)
    logger.info("Anti-aliasing ENABLED")
# pg.setConfigOption('leftButtonPan', False)
# see also ViewBox.setMouseMode(ViewBox.RectMode)
# file:///usr/share/doc/python-pyqtgraph-doc/html/graphicsItems/viewbox.html#pyqtgraph.ViewBox.setMouseMode


def _preferred_trend_for_span(span):
    if span > const.TREND_MAX_SECONDS['sec']:
        return 'min'
    elif span > const.TREND_MAX_SECONDS['raw']:
        return 'sec'
    else:
        return 'raw'


LABEL_CSS_FMT = """
font-size: {font_size}px;
color: {color};
background: {background};
padding-bottom: 6;
"""

##################################################


Ui_MainWindow, QMainWindow = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), 'scope.ui'))


class NDScope(QMainWindow, Ui_MainWindow):
    def __init__(self, layout):
        """initilize NDScope object

        `layout` should be a list of subplot definitions, each being a
        dictionary matching the keyword arguments to add_plot()
        (e.g. row, col, colspan, channels, etc.).

        """
        super(NDScope, self).__init__(None)
        self.setupUi(self)

        # FIXME: HACK: this is an attempt to bypass the following bug:
        #
        # Traceback (most recent call last):
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/GraphicsObject.py", line 23, in itemChange
        #     self.parentChanged()
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/GraphicsItem.py", line 440, in parentChanged
        #     self._updateView()
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/GraphicsItem.py", line 492, in _updateView
        #     self.viewRangeChanged()
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/PlotDataItem.py", line 671, in viewRangeChanged
        #     self.updateItems()
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/PlotDataItem.py", line 483, in updateItems
        #     x,y = self.getData()
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/PlotDataItem.py", line 561, in getData
        #     if view is None or not view.autoRangeEnabled()[0]:
        # AttributeError: 'GraphicsLayoutWidget' object has no attribute 'autoRangeEnabled'
        def autoRangeEnabled():
            return False, False
        self.graphView.autoRangeEnabled = autoRangeEnabled

        ##########

        self.data = DataStore()
        self.last_fetch_cmd = None
        self.last_data = (None, 0, 0)

        self.Crosshair = Crosshair()
        self.TCursors = TCursors()
        self.YCursors = YCursors()

        self.plots = []
        rows = 0
        for subplot in layout:
            self.add_plot(**subplot)
            rows = max(rows, subplot['row'])

        # self.referenceTimeLabel = self.graphView.addLabel(
        #     "GPS Time",
        #     size='11pt',
        #     #bold=True,
        #     row=rows+1,
        #     col=0,
        #     colspan=1000,
        # )
        # colspan is an arbitrarily large number just so that the
        # label always spans the full width regardless what it is
        white = '#FFFFFF'
        black = '#000000'
        color = white
        background = black
        if pg.getConfigOption('background') == 'w':
            color = black
            background = white
        label_css = LABEL_CSS_FMT.format(font_size='{font_size}', color=color, background=background)
        self.referenceTimeLabel.setStyleSheet(label_css.format(font_size=16))
        self.spanLabel.setStyleSheet(label_css.format(font_size=14))
        self.spanLabel.adjustSize()
        self.set_t0(util.gpstime_parse('now').gps())

        ##########
        # data retrieval

        self.data.signal_data_retrieve_start.connect(self._data_retrieve_start)
        self.data.signal_data.connect(self._update_plots)
        self.data.signal_data_retrieve_done.connect(self._data_retrieve_done)

        ##########
        # window/range entry

        def NonEmptyValidator():
            return QtGui.QRegularExpressionValidator(QtCore.QRegularExpression('.+'))

        self.entryT0GPS.textEdited.connect(self.update_entryT0Greg)
        self.entryT0GPS.setValidator(QtGui.QDoubleValidator())
        self.entryT0GPS.returnPressed.connect(self.update_t0)

        self.entryT0Greg.textEdited.connect(self.update_entryT0GPS)
        self.entryT0Greg.setValidator(NonEmptyValidator())
        self.entryT0Greg.returnPressed.connect(self.update_t0)

        self.buttonT0Now.clicked.connect(self.set_entryT0)

        self.entryWindowStart.setValidator(QtGui.QDoubleValidator())
        self.entryWindowStart.returnPressed.connect(self.update_window)

        self.entryWindowEnd.setValidator(QtGui.QDoubleValidator())
        self.entryWindowEnd.returnPressed.connect(self.update_window)

        self.entryStartGPS.textEdited.connect(self.update_entryStartGreg)
        self.entryStartGPS.setValidator(QtGui.QDoubleValidator())
        self.entryStartGPS.returnPressed.connect(self.update_range)

        self.entryStartGreg.textEdited.connect(self.update_entryStartGPS)
        self.entryStartGreg.setValidator(NonEmptyValidator())
        self.entryStartGreg.returnPressed.connect(self.update_range)

        self.entryEndGPS.setValidator(QtGui.QDoubleValidator())
        self.entryEndGPS.textEdited.connect(self.update_entryEndGreg)
        self.entryEndGPS.returnPressed.connect(self.update_range)

        self.entryEndGreg.textEdited.connect(self.update_entryEndGPS)
        self.entryEndGreg.setValidator(NonEmptyValidator())
        self.entryEndGreg.returnPressed.connect(self.update_range)

        self.buttonEndNow.clicked.connect(self.set_entryEnd)

        self.fetchButton1.clicked.connect(self.update_t0)
        self.fetchButton2.clicked.connect(self.update_range)

        ##########
        # trigger

        self.trigger = Trigger()
        self.trigger.sigLevelChanged.connect(self.update_trigger_level)
        self.triggerLevel.setValidator(QtGui.QDoubleValidator())
        self.triggerLevel.returnPressed.connect(self.set_trigger_level)
        self.triggerResetLevel.clicked.connect(self.reset_trigger_level)
        self.triggerSingle.clicked.connect(self.trigger.set_single)
        self.triggerInvert.clicked.connect(self.trigger.set_invert)
        self.triggerGroup.toggled.connect(self.toggle_trigger)

        ##########
        # cursors

        self.crosshairGroup.toggled.connect(self.toggle_crosshair)

        self.cursorTGroup.toggled.connect(self.toggle_t_cursors)
        self.cursorTReset.clicked.connect(self.TCursors.reset)

        self.cursorYGroup.toggled.connect(self.toggle_y_cursors)
        self.cursorYPlot.currentIndexChanged.connect(self.set_y_cursors_to_selected)
        self.cursorYReset.clicked.connect(self.YCursors.reset)

        ##########
        # export

        self.dialogWidget = QFileDialog()
        self.dialogWidget.setAcceptMode(QFileDialog.AcceptSave)
        self.dialogWidget.setMimeTypeFilters(["image/png"])
        self.dialogWidget.setDefaultSuffix("png")
        self.dialogWidget.fileSelected.connect(self.exportPNG)
        self.exportButton.clicked.connect(self.dialogWidget.show)

        self.dialogWidget2 = QFileDialog()
        self.dialogWidget2.setAcceptMode(QFileDialog.AcceptSave)
        self.dialogWidget2.setMimeTypeFilters(["image/png"])
        self.dialogWidget2.setDefaultSuffix("png")
        self.dialogWidget2.fileSelected.connect(self.exportPNGPath.setText)
        self.exportPNGPathButton.clicked.connect(self.dialogWidget2.show)
        self.exportButton2.clicked.connect(self.exportPNG)

        ##########

        self.controlBar.hide()
        self.controlExpandButton.setIcon(self.style().standardIcon(QStyle.SP_TitleBarShadeButton))
        self.controlExpandButton.setText('')
        self.controlExpandButton.clicked.connect(self._controlExpand)
        self.controlCollapseButton.setIcon(self.style().standardIcon(QStyle.SP_TitleBarUnshadeButton))
        self.controlCollapseButton.setText('')
        self.controlCollapseButton.clicked.connect(self._controlCollapse)

        self.startstopButton.clicked.connect(self.startstop)
        self.startstopButton2.clicked.connect(self.startstop)
        self.resetRangeButton.clicked.connect(self.reset_range)
        self.resetRangeButton2.clicked.connect(self.reset_range)
        self.resetT0Button.clicked.connect(self.reset_t0)
        self.resetT0Button2.clicked.connect(self.reset_t0)

        self.statusBar.addWidget(QtWidgets.QLabel(
            "ndscope {}    NDS server: {}".format(
                __version__,
                os.getenv('NDSSERVER'),
            )))

        ##########

        self._data_retrieve_done(None)

        #self.plot0.sigXRangeChanged.connect(self.update_mouse)
        self.update_mouse_proxy = pg.SignalProxy(
            self.plot0.sigXRangeChanged,
            rateLimit=1,
            slot=self.update_mouse,
        )

    ##########

    def _controlExpand(self):
        self.controlBarSmall.hide()
        self.controlBar.show()

    def _controlCollapse(self):
        self.controlBar.hide()
        self.controlBarSmall.show()

    ##########

    def add_plot(self, channels=None,
                 row=None, col=None, colspan=1,
                 yrange=None):
        """Add plot to the scope

        If provided `channels` should be a list of channel:property
        dicts to add to the plot on initialization.

        """
        logger.info("creating plot ({}, {})...".format(row, col))

        plot = NDScopePlot()
        plot.channel_added.connect(self._channel_added)
        plot.channel_removed.connect(self._channel_removed)
        plot.new_plot_request.connect(self.add_plot)
        plot.remove_plot_request.connect(self.remove_plot)

        # tie all plot x-axes together
        if self.plots:
            plot.setXLink(self.plot0)

        # set y ranges
        if yrange in [None, 'auto']:
            plot.enableAutoRange(axis='y')
        else:
            plot.setYRange(*yrange)

        self.plots.append(plot)

        self.TCursors.add_plot(plot)
        self.cursorYPlot.clear()
        self.cursorYPlot.addItems([str(i) for i, p in enumerate(self.plots)])

        if channels:
            # each channel should be a {name: curve_params} dict
            for chan in channels:
                name, kwargs = list(chan.items())[0]
                kwargs = kwargs or {}
                plot.add_channel(name, **kwargs)

        # FIXME: where to add if row/col not specified?  need some
        # sort of layout policy
        self.graphView.addItem(
            plot,
            row=row, col=col,
            colspan=colspan,
        )

        return plot

    def remove_plot(self, plot):
        """remove plot from layout"""
        if len(self.plots) == 1:
            return
        # first remove all channels
        # make copy of list cause we'll be changing it
        for chan in list(plot.channels.keys()):
            plot.remove_channel(chan)
        self.graphView.removeItem(plot)

    @property
    def plot0(self):
        return self.plots[0]

    def plots4chan(self, channel):
        """Return list of plots displaying channel"""
        plots = []
        for plot in self.plots:
            if channel in plot.channels:
                plots.append(plot)
        return plots

    def _update_triggerSelect(self):
        try:
            self.triggerSelect.currentIndexChanged.disconnect(self.update_trigger_channel)
        except TypeError:
            pass
        self.triggerSelect.clear()
        self.triggerSelect.addItems(self.data.channels)
        self.triggerSelect.currentIndexChanged.connect(self.update_trigger_channel)

    def _channel_added(self, channel):
        self.data.add_channel(str(channel))
        self._update_triggerSelect()

    def _channel_removed(self, channel):
        self.data.remove_channel(str(channel))
        self._update_triggerSelect()

    ##########

    def get_window(self):
        (xmin, xmax), (ymin, ymax) = self.plot0.viewRange()
        return xmin, xmax

    def get_range(self):
        """tuple of (start, end) times"""
        xmin, xmax = self.get_window()
        start = self.t0 + xmin
        end = self.t0 + xmax
        return start, end

    def get_span(self):
        """time span in seconds"""
        start, end = self.get_range()
        return abs(end - start)

    def preferred_trend(self):
        """preferred trend for the current time span"""
        span = self.get_span()
        return _preferred_trend_for_span(span)

    ##########

    def start(self, window=None):
        """Start online mode

        """
        logger.info('START')
        self.crosshairGroup.setEnabled(False)
        self.toggle_crosshair(checked=False)
        if window:
            span = abs(min(window))
        else:
            span = self.get_span()
            window = (-span, 0)
        self.set_window(*window)
        trend = self.preferred_trend()
        self.data.start_online(trend, span)

    def stop(self, message=None):
        """Stop online mode

        """
        logger.info('STOP')
        self.data.terminate()

    def startstop(self):
        """toggle start/stop"""
        if self.data.active:
            self.stop()
        else:
            self.start()

    def _data_request(self, force=False):
        ltrend, lstart, lend = self.last_data
        trend = self.preferred_trend()
        start, end = self.get_range()
        if not force \
           and trend == ltrend \
           and start >= lstart \
           and end <= lend:
            return
        self.data.request(trend, (start, end))

    def _fetch(self, **kwargs):
        if 't0' in kwargs:
            t0 = kwargs['t0']
            window = kwargs['window']
            start = t0 + window[0]
            end = t0 + window[1]
        else:
            start = kwargs['start']
            end = kwargs['end']
            t0 = (end+start)/2
            window = (t0-start, t0-end)
        logger.info('FETCH: {}'.format((start, end)))
        self.triggerGroup.setChecked(False)
        self.set_t0(t0)
        self.set_window(window[0], window[1])
        self._data_request(force=True)

    def fetch(self, **kwargs):
        """Fetch data offline

        May specify `t0` and `window`, or `start` and `end`.

        """
        self.last_fetch_cmd = kwargs
        self._fetch(**kwargs)

    def reset_range(self):
        """Reset to last fetch range

        """
        logger.info('RESET')
        for plot in self.plots:
            plot.enableAutoRange(axis='y')
        if self.data.online:
            span = self.get_span()
            self.set_window(-span, 0)
        elif self.last_fetch_cmd:
            self._fetch(**self.last_fetch_cmd)

    def update_mouse(self):
        """update time range on mouse pan/zoom"""
        self.updateGPS(update_window=True)
        if not self.updateOnRange.isChecked():
            return
        self.update_span_label()
        self._data_request()

    def get_entryWindow(self):
        try:
            window = (
                float(self.entryWindowStart.text()),
                float(self.entryWindowEnd.text()),
            )
        except ValueError:
            return
        self.last_window = window
        return window

    def update_t0(self):
        self.update_entryT0GPS()
        try:
            t0 = float(self.entryT0GPS.text())
            window = self.get_entryWindow()
        except ValueError:
            return
        self._fetch(t0=t0, window=window)

    def update_window(self):
        self.set_window(*self.get_entryWindow())

    def update_range(self):
        self.update_entryStartGPS()
        self.update_entryEndGPS()
        try:
            start = float(self.entryStartGPS.text())
            end = float(self.entryEndGPS.text())
        except ValueError:
            return
        self._fetch(start=start, end=end)

    # SLOT
    def _data_retrieve_start(self, msg):
        self.statusBar.setStyleSheet("background: rgba(0,100,0,100);")
        self.statusBar.showMessage(msg)
        self.startstopButton.setText("stop")
        self.startstopButton.setIcon(self.style().standardIcon(QStyle.SP_MediaStop))
        self.startstopButton2.setText("stop")
        self.startstopButton2.setIcon(self.style().standardIcon(QStyle.SP_MediaStop))
        self.rangeTab.setEnabled(False)
        self.entryT0GPS.setEnabled(False)
        self.entryT0Greg.setEnabled(False)
        self.buttonT0Now.setEnabled(False)
        self.fetchButton1.setEnabled(False)
        self.fetchButton2.setEnabled(False)
        self.resetT0Button.setEnabled(False)
        self.resetT0Button2.setEnabled(False)
        self.exportButton.setEnabled(False)
        self.exportTab.setEnabled(False)

    # SLOT
    def _data_retrieve_done(self, error):
        if error:
            self.statusBar.setStyleSheet("background: rgba(255,0,0,255); color: black; font-weight: bold;")
            self.statusBar.showMessage(error)
        else:
            self.statusBar.setStyleSheet("")
            self.statusBar.clearMessage()
        # for plot in self.plots:
        #     plot.disableAutoRange(axis='y')
        self.startstopButton.setText("online")
        self.startstopButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.startstopButton2.setText("online")
        self.startstopButton2.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.rangeTab.setEnabled(True)
        self.entryT0GPS.setEnabled(True)
        self.entryT0Greg.setEnabled(True)
        self.buttonT0Now.setEnabled(True)
        self.fetchButton1.setEnabled(True)
        self.fetchButton2.setEnabled(True)
        self.resetT0Button.setEnabled(True)
        self.resetT0Button2.setEnabled(True)
        self.crosshairGroup.setEnabled(True)
        if self.crosshairGroup.isChecked():
            self.toggle_crosshair(checked=True)
        self.exportButton.setEnabled(True)
        self.exportTab.setEnabled(True)

    ##########
    # PLOTTING

    # SLOT
    def _update_plots(self, recv):
        logger.log(5, f"PLOT: {recv}")

        data, trend, online = recv

        if not data:
            logger.log(5, "CLEAR")
            for plot in self.plots:
                plot.clear_data()
            return

        self.last_data = (trend,) + data.range

        trigger = None
        if online:
            # if we're online, check for triggers
            if self.trigger.active:
                trigger = self.trigger.check(data)
                if trigger:
                    self.update_trigger_time(trigger)
                    self.set_t0(trigger)

            else:
                self.set_t0(data.range[1])

        for plot in self.plots:
            plot.update(data, self.t0)

        if trigger and self.trigger.single:
            self.stop()

    def update_tlabel(self):
        self.referenceTimeLabel.setText(
            't0 = {} [{:0.4f}]'.format(
                util.gpstime_str_greg(
                    util.gpstime_parse(self.t0),
                    fmt=const.DATETIME_FMT,
                ),
                self.t0,
            )
        )

    def update_span_label(self):
        sep = '='
        span = self.get_span()
        try:
            prec = int(np.round(np.log10(span))) - 2
        except OverflowError:
            span = 2.385e-07
            prec = -9
            sep = '<'
        self.spanLabel.setText(
            'span {} {}'.format(
                sep,
                util.seconds_time_str(span, prec),
            ))
        self.spanLabel.adjustSize()

    def set_t0(self, t0):
        self.t0 = t0
        logger.log(5, f"t0 = {t0}")
        self.update_tlabel()
        self.updateGPS(update_window=False)

    def reset_t0(self):
        start, end = self.get_range()
        t0 = (start+end)/2
        xd = (end-start)/2
        self._fetch(t0=t0, window=(-xd, xd))

    def set_window(self, xmin, xmax):
        logger.debug('RANGE: {} {}'.format(xmin, xmax))
        try:
            self.plot0.sigXRangeChanged.disconnect(self.update_mouse)
        except TypeError:
            pass
        self.plot0.setXRange(xmin, xmax, padding=0, update=False)
        self.update_span_label()
        self.plot0.sigXRangeChanged.connect(self.update_mouse)

    ##########
    # TIMES

    def updateGPS(self, update_window=False):
        start, end = self.get_range()
        self.set_entryT0(self.t0)
        self.set_entryStart(start)
        self.set_entryEnd(end)
        if update_window:
            self.set_entryWindow()

    def set_entryT0(self, time=None):
        if time:
            gt = util.gpstime_parse(time)
        else:
            gt = util.gpstime_parse('now')
        self.entryT0GPS.setText(util.gpstime_str_gps(gt))
        self.entryT0Greg.setText(util.gpstime_str_greg(gt))

    def update_entryT0GPS(self):
        gt = util.gpstime_parse(self.entryT0Greg.text())
        if not gt:
            return
        self.entryT0GPS.setText(util.gpstime_str_gps(gt))

    def update_entryT0Greg(self):
        gt = util.gpstime_parse(self.entryT0GPS.text())
        if not gt:
            return
        self.entryT0Greg.setText(util.gpstime_str_greg(gt))

    def set_entryWindow(self):
        xmin, xmax = self.get_window()
        self.entryWindowStart.setText(str(xmin))
        self.entryWindowEnd.setText(str(xmax))

    def set_entryStart(self, time):
        gt = util.gpstime_parse(time)
        self.entryStartGPS.setText(util.gpstime_str_gps(gt))
        self.entryStartGreg.setText(util.gpstime_str_greg(gt))

    def set_entryEnd(self, time=None):
        if time:
            gt = util.gpstime_parse(time)
        else:
            gt = util.gpstime_parse('now')
        self.entryEndGPS.setText(util.gpstime_str_gps(gt))
        self.entryEndGreg.setText(util.gpstime_str_greg(gt))

    def update_entryStartGPS(self):
        t = self.entryStartGreg.text()
        gt = util.gpstime_parse(t)
        if not gt:
            return
        self.entryStartGPS.setText(util.gpstime_str_gps(gt))

    def update_entryStartGreg(self):
        t = self.entryStartGPS.text()
        gt = util.gpstime_parse(t)
        if not gt:
            return
        self.entryStartGreg.setText(util.gpstime_str_greg(gt))

    def update_entryEndGPS(self):
        t = self.entryEndGreg.text()
        gt = util.gpstime_parse(t)
        if not gt:
            return
        self.entryEndGPS.setText(util.gpstime_str_gps(gt))

    def update_entryEndGreg(self):
        t = self.entryEndGPS.text()
        gt = util.gpstime_parse(t)
        if not gt:
            return
        self.entryEndGreg.setText(util.gpstime_str_greg(gt))

    ##########
    # TRIGGER

    def set_trigger(self, channel):
        assert channel in self.data.channels + [None]
        if channel == self.trigger.channel:
            return
        if self.trigger.channel is not None:
            tplot = self.plots4chan(self.trigger.channel)[0]
        else:
            tplot = None
        if channel is not None:
            nplot = self.plots4chan(channel)[0]
        else:
            nplot = None
        self.trigger.channel = channel
        if nplot != tplot:
            if tplot:
                tplot.removeItem(self.trigger.line)
            if nplot:
                nplot.addItem(self.trigger.line, ignoreBounds=True)
                nplot.disableAutoRange(axis='y')
            return True
        else:
            return False

    def update_trigger_level(self):
        self.triggerLevel.setText('{:g}'.format(self.trigger.level))

    def set_trigger_level(self):
        value = float(self.triggerLevel.text())
        self.trigger.set_level(value)

    def reset_trigger_level(self):
        chan = self.trigger.channel
        if self.data['raw'] and chan in self.data['raw']:
            y = self.data['raw'][chan].data['raw']
            yn = y[np.where(np.invert(np.isnan(y)))[0]]
            value = np.mean(yn)
        else:
            value = 0
        self.trigger.set_level(value)

    def update_trigger_time(self, time):
        self.triggerTime.setText('{:14.6f}'.format(time))

    def update_trigger_channel(self):
        chan = str(self.triggerSelect.currentText())
        if self.set_trigger(chan):
            self.reset_trigger_level()
        logger.info("trigger set: {}".format(chan))

    def toggle_trigger(self, checked=None):
        if not self.trigger:
            return
        if checked is None:
            checked = self.triggerGroup.isChecked()
        if checked:
            chan = str(self.triggerSelect.currentText())
            self.set_trigger(chan)
            self.reset_trigger_level()
            span = self.get_span()
            self.set_window(-span/2, span/2)
            logger.info("trigger enabled")
        else:
            self.set_trigger(None)
            span = self.get_span()
            self.set_window(-span, 0)
            logger.info("trigger disabled")

    ##########
    # CURSORS

    def toggle_crosshair(self, checked=None):
        if checked is None:
            checked = self.crosshairGroup.isChecked()
        if checked:
            for plot in self.plots:
                plot.disableAutoRange(axis='y')
            # self.graphView.scene().sigMouseMoved.connect(self.Crosshair.update)
            self.crosshair_proxy = pg.SignalProxy(
                self.graphView.scene().sigMouseMoved,
                rateLimit=60,
                slot=self.update_crosshair)
            logger.info("crosshair enabled")
        else:
            self.Crosshair.set_active_plot(None)
            self.crosshair_proxy = None
            logger.info("crosshair disabled")

    def update_crosshair(self, event):
        # using signal proxy unfortunately turns the original
        # arguments into a tuple pos = event
        pos = event[0]
        for plot in self.plots:
            if plot.sceneBoundingRect().contains(pos):
                self.Crosshair.set_active_plot(plot)
                x = plot.vb.mapSceneToView(pos).x()
                t = self.t0 + x
                y = plot.vb.mapSceneToView(pos).y()
                self.Crosshair.update(x, y, t)
                return

    def toggle_t_cursors(self, checked=None):
        if checked is None:
            checked = self.cursorTGroup.isChecked()
        if checked:
            self.TCursors.enable()
            logger.info("T-cursor enabled")
        else:
            self.TCursors.disable()
            logger.info("T-cursor disabled")

    def get_selected_y_cursors_plot(self):
        ind = self.cursorYPlot.currentText()
        if ind:
            return self.plots[int(ind)]

    def set_y_cursors_to_selected(self):
        if not self.cursorYGroup.isChecked():
            return
        plot = self.get_selected_y_cursors_plot()
        if plot:
            self.YCursors.set_plot(plot)

    def toggle_y_cursors(self, checked=None):
        if checked is None:
            checked = self.cursorYGroup.isChecked()
        if checked:
            self.set_y_cursors_to_selected()
            logger.info("Y-cursor enabled")
        else:
            self.YCursors.set_plot(None)
            logger.info("Y-cursor disabled")

    ##########
    # EXPORT

    def exportPNG(self, path=None):
        if not path:
            path = self.exportPNGPath.text()
        else:
            self.exportPNGPath.setText(path)
        widget = self.plotWindow
        image = QImage(widget.size(), QImage.Format_RGB32)
        painter = QPainter(image)
        font = painter.font()
        font.setPixelSize(48)
        widget.render(painter)
        painter.end()
        if image.save(path, 'PNG'):
            msg = f"exported plot to file: {path}"
            logger.info(msg)
            self.statusBar.showMessage(msg, 7000)
        else:
            msg = "Failed to write image to file:"
            logger.error(msg + ' ' + path)
            error = QtWidgets.QMessageBox()
            error.setWindowTitle("File write error")
            error.setIcon(QtWidgets.QMessageBox.Critical)
            error.setText(msg)
            error.setInformativeText(path)
            error.exec_()
